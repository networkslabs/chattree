import chattree.ChatNode;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Application {
    public static void main(String[] args) throws IOException {
        if (args.length == 3) {
            new ChatNode(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]), null).run();
        } else if (args.length == 5) {
            new ChatNode(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]), new InetSocketAddress(args[3], Integer.parseInt(args[4]))).run();
        } else {
            System.out.println("Usage: name port lossrate parentIp parent port (optional) ");
        }
    }
}
