package chattree;

import chattree.utils.LossSimulator;
import chattree.utils.MessageBuilder;
import chattree.utils.protocol.ChatProto.Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class ChatNode implements Runnable {
    static final int MAX_DATA_LENGTH_BYTES = 512;
    private final String name;

    private final LossSimulator lossSimulator;

    private final DatagramSocket socket;
    //Храним в мапе живых соседей.
    private final Map<InetSocketAddress, Long> addressMap = new ConcurrentHashMap<>();
    //Заместители узлов
    private final Map<InetSocketAddress, InetSocketAddress> neighboursDeputies = new ConcurrentHashMap<>();
    //Очередь отправленных сообщений, ожидающая подтверждений
    private final Map<String, DatagramPacket> sentPull = new ConcurrentHashMap<>();

    private final int LIFE_TIME_MILLIS = 2000;
    private InetSocketAddress deputy = null;


    public ChatNode(String name, int port, int loses, InetSocketAddress neighbour) throws IOException {
        this.name = name;
        this.lossSimulator = new LossSimulator(loses);
        this.socket = new DatagramSocket(port);

        System.out.println(name + " " + port);

        if (neighbour != null) {
            Message connectMessage = MessageBuilder.createConnectMessage(this.name);

            var messageBytes = connectMessage.toByteArray();

            DatagramPacket packet = new DatagramPacket(messageBytes, messageBytes.length);
            packet.setSocketAddress(neighbour);
            this.socket.send(packet);
            sentPull.put(connectMessage.getAnswerableMessage().getGuid(), packet);
            updateNeighbours(neighbour);
        }
    }

    private void sendMessage(DatagramPacket packet) {
        try {
            this.socket.send(packet);
        } catch (IOException ignored) {
        }
    }

    private void resendMessages() {
        sentPull.forEach((guid, packet) -> {
            sendMessage(packet);
        });
    }

    private void chooseDeputy() {
        if (this.deputy == null) {
            this.deputy = addressMap.keySet().stream().findFirst().orElse(null);
        }
    }

    private void sendDeputyMessage(SocketAddress address) {
        chooseDeputy();

        if (this.deputy == null) return;

        if (this.deputy.equals(address)) {
            return;
        }

        Message deputyMessage = MessageBuilder.createDeputyMessage(this.deputy.getHostName() + ":" + this.deputy.getPort());
        var messageBytes = deputyMessage.toByteArray();
        DatagramPacket packet = new DatagramPacket(messageBytes, messageBytes.length);
        packet.setSocketAddress(address);
        sendMessage(packet);
        sentPull.put(deputyMessage.getAnswerableMessage().getGuid(), packet);
    }

    private void updateNeighbours(InetSocketAddress address) {
        Long currentTime = System.currentTimeMillis();

        if (addressMap.containsKey(address)) {
            addressMap.replace(address, currentTime);
        } else {
            //Если новый  - то говорим ему кто наш зам.
            sendDeputyMessage(address);
            addressMap.put(address, currentTime);
            System.out.println(address.getHostName() + ":" + address.getPort() + " connected");
        }
    }

    @Override
    public void run() {
        new Receiver().start();
        new NetworkStructureController().start();
        new Thread(this::userInput).start();
    }


    private void userInput() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try (reader) {
            while (Thread.currentThread().isAlive()) {
                String input = "";
                input = reader.readLine();

                if (!input.equals("")) {
                    var message = MessageBuilder.createTextMessage(this.name, input);
                    var data = message.toByteArray();
                    DatagramPacket packet = new DatagramPacket(data, data.length);

                    addressMap.forEach((addr, time) -> {
                        packet.setSocketAddress(addr);
                        sendMessage(packet);
                        sentPull.put(message.getAnswerableMessage().getGuid(), packet);
                    });
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class NetworkStructureController extends Thread {
        private final byte[] ping = MessageBuilder.createPingMessage().toByteArray();
        private final DatagramPacket pingPacket = new DatagramPacket(ping, ping.length);


        private void checkDeadNodes(Long currentTime) {
            addressMap.forEach((addr, time) -> {
                if (currentTime - time > LIFE_TIME_MILLIS) {
                    if (addr == deputy) {
                        deputy = null;
                        //если умер зам обновить
                        addressMap.forEach((ad, ignored) -> {
                            sendDeputyMessage(ad);
                        });
                    }

                    System.out.println(addr.getHostName() + ":" + addr.getPort() + " disconnected");
                    //Нужно реконектиться к заместителю, если он назначен

                    var deputyNode = neighboursDeputies.getOrDefault(addr, null);

                    if (deputyNode != null) {
                        System.out.println("Connecting to deputy: " + deputyNode.getHostName() + ":" + deputyNode.getPort());

                        var connectMessage = MessageBuilder.createConnectMessage(name);
                        var messageBytes = connectMessage.toByteArray();

                        DatagramPacket packet = new DatagramPacket(messageBytes, messageBytes.length);
                        packet.setSocketAddress(deputyNode);
                        sendMessage(packet);
                        sentPull.put(connectMessage.getAnswerableMessage().getGuid(), packet);
                        updateNeighbours(deputyNode);
                    }

                    addressMap.remove(addr);
                    neighboursDeputies.remove(addr);

                    //Мертвому не нужно отправлять пакеты
                    sentPull.forEach((guid, packet) -> {
                        if (packet.getSocketAddress().equals(addr)) {
                            sentPull.remove(guid);
                        }
                    });

                }
            });
        }


        @Override
        public void run() {
            while (Thread.currentThread().isAlive()) {

                addressMap.forEach((addr, time) -> {
                    pingPacket.setSocketAddress(addr);
                    sendMessage(pingPacket);
                });

                try {
                    Thread.sleep(LIFE_TIME_MILLIS / 8);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //Проверяем узлы и повторяем не подтвержденные сообщения
                checkDeadNodes(System.currentTimeMillis());

                resendMessages();
            }
        }
    }

    private class Receiver extends Thread {

        private final Map<String, Long> textMessegesMap = new ConcurrentHashMap<>();
        InetSocketAddress lastReceivedAddress;

        private void deleteOldMessages() {
            long currentTime = System.currentTimeMillis();

            textMessegesMap.forEach((guid, time) -> {

                if (currentTime - time > 4 * LIFE_TIME_MILLIS) {
                    textMessegesMap.remove(guid);
                }

            });

        }

        private void sendOk(String guid) {
            var ok = MessageBuilder.createOkMessage(guid).toByteArray();
            DatagramPacket packet = new DatagramPacket(ok, ok.length);
            packet.setSocketAddress(lastReceivedAddress);
            ChatNode.this.sendMessage(packet);
        }

        private InetSocketAddress parseSocketAddr(String address) {

            var addr = address.split(":");

            var ip = addr[0];
            var port = Integer.parseInt(addr[1]);


            return new InetSocketAddress(ip, port);
        }

        private void forAnswerableMessage(Message message) {
            var answerableMessage = message.getAnswerableMessage();
            sendOk(answerableMessage.getGuid());

            deleteOldMessages();
            if (textMessegesMap.containsKey(answerableMessage.getGuid())) {
                updateNeighbours(lastReceivedAddress);
                return;
            }
            textMessegesMap.put(answerableMessage.getGuid(), System.currentTimeMillis());


            if (answerableMessage.hasConnectMessage()) {
                var connectMessage = answerableMessage.getConnectMessage();
                System.out.println("New user " + connectMessage.getName());

                neighboursDeputies.forEach((addr, alt) -> {
                    if (addr.equals(lastReceivedAddress) || alt.equals(lastReceivedAddress)) {
                        neighboursDeputies.remove(addr);
                    }
                });

            } else if (answerableMessage.hasDeputyNodeMessage()) {
                var deputyMessage = answerableMessage.getDeputyNodeMessage();
                var address = parseSocketAddr(deputyMessage.getAddress());

                if (neighboursDeputies.containsKey(lastReceivedAddress)) {
                    neighboursDeputies.replace(lastReceivedAddress, address);
                } else {
                    neighboursDeputies.put(lastReceivedAddress, address);
                }


            } else if (answerableMessage.hasTextMessage()) {

                var textMessage = answerableMessage.getTextMessage();

                System.out.println("[" + textMessage.getFrom() + "]: " + textMessage.getMessage());

                //Рассылаем дальше по дереву
                addressMap.forEach((addr, time) -> {

                    var newMessage = MessageBuilder.createTextMessage(textMessage.getFrom(), textMessage.getMessage());
                    var data = newMessage.toByteArray();
                    DatagramPacket packet = new DatagramPacket(data, data.length);

                    if (!lastReceivedAddress.equals(addr)) {
                        packet.setSocketAddress(addr);
                        sendMessage(packet);
                        sentPull.put(newMessage.getAnswerableMessage().getGuid(), packet);
                    }
                });

            }

            updateNeighbours(lastReceivedAddress);
        }

        private void forNonAnswerableMessage(Message message) {

            ///Сообщение без ответа не может придти от неподключенного юзера так что игнорируем
            if (!addressMap.containsKey(lastReceivedAddress)) {
                return;
            }

            updateNeighbours(lastReceivedAddress);

            var nonAnswerableMessage = message.getNonAnswerableMessage();

            if (nonAnswerableMessage.hasOkMessage()) {
                var guid = nonAnswerableMessage.getOkMessage().getReceivedGuid();
                sentPull.remove(guid);
            }
        }

        @Override
        public void run() {
            while (Thread.currentThread().isAlive()) {

                try {
                    byte[] data = new byte[MAX_DATA_LENGTH_BYTES];
                    DatagramPacket packet = new DatagramPacket(data, data.length);
                    ChatNode.this.socket.receive(packet);

                    if (ChatNode.this.lossSimulator.isLost()) {
                        continue;
                    }


                    Message message = Message.parseFrom(Arrays.copyOfRange(data, 0, packet.getLength()));

                    SocketAddress packetAdd = packet.getSocketAddress();

                    ///Если пакет пришел не по InetSocketAddress игнорируем его
                    if (!(packetAdd instanceof InetSocketAddress)) {
                        continue;
                    }

                    lastReceivedAddress = (InetSocketAddress) packetAdd;


                    if (message.hasAnswerableMessage()) {
                        forAnswerableMessage(message);
                    } else if (message.hasNonAnswerableMessage()) {
                        forNonAnswerableMessage(message);
                    }

                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        }
    }
}
