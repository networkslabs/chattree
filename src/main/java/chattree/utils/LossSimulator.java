package chattree.utils;

import java.util.Random;

public class LossSimulator {
    private static final int MAX_BOUND_LOSS = 100;
    private final Random random = new Random(System.currentTimeMillis());
    private int lossrate = 0;


    public LossSimulator(int lossrate) {
        this.lossrate = lossrate;
    }

    public boolean isLost() {
        if (lossrate == 0) {
            return false;
        }
        if (lossrate == 100) {
            return true;
        }

        int x = random.nextInt(MAX_BOUND_LOSS);
        return (x < lossrate);
    }

}
