package chattree.utils;

import chattree.utils.protocol.ChatProto.AnswerableMessage;
import chattree.utils.protocol.ChatProto.Message;
import chattree.utils.protocol.ChatProto.NonAnswerableMessage;

import java.util.UUID;

public class MessageBuilder {

    public static Message createConnectMessage(String name) {
        var connectMessage = AnswerableMessage
                .ConnectMessage.newBuilder()
                .setName(name);

        var message = AnswerableMessage.newBuilder()
                .setConnectMessage(connectMessage)
                .setGuid(UUID.randomUUID().toString());

        return Message.newBuilder().setAnswerableMessage(message).build();
    }

    public static Message createDeputyMessage(String address) {
        var deputyNodeMessage = AnswerableMessage
                .DeputyNodeMessage.newBuilder()
                .setAddress(address);

        var message = AnswerableMessage.newBuilder()
                .setDeputyNodeMessage(deputyNodeMessage)
                .setGuid(UUID.randomUUID().toString());

        return Message.newBuilder().setAnswerableMessage(message).build();
    }

    public static Message createPingMessage() {
        return Message.newBuilder()
                .setNonAnswerableMessage(
                        NonAnswerableMessage.newBuilder()
                                .setPingMessage(NonAnswerableMessage.PingMessage.newBuilder())
                ).build();
    }

    public static Message createOkMessage(String guid) {
        return Message.newBuilder()
                .setNonAnswerableMessage(
                        NonAnswerableMessage.newBuilder()
                                .setOkMessage(NonAnswerableMessage.OkMessage.newBuilder()
                                        .setReceivedGuid(guid))
                ).build();
    }

    public static Message createTextMessage(String name, String text) {
        var textMessage = AnswerableMessage.TextMessage.newBuilder()
                .setMessage(text)
                .setFrom(name);

        var message = AnswerableMessage.newBuilder()
                .setTextMessage(textMessage)
                .setGuid(UUID.randomUUID().toString());

        return Message.newBuilder().setAnswerableMessage(message).build();
    }
}
